#!/bin/bash

[ "$DEBUG" = "true" ] && set -x

# Configure composer
[ ! -z "${COMPOSER_VERSION}" ] && \
    composer self-update $COMPOSER_VERSION

exec "$@"
