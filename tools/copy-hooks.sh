#!/bin/sh

if [ -d '.git/hooks' ]
then
  \cp tools/git-hooks/pre-commit.sh .git/hooks/pre-commit
  chmod +x .git/hooks/pre-commit
  \cp tools/git-hooks/pre-push.sh .git/hooks/pre-push
  chmod +x .git/hooks/pre-push
fi
