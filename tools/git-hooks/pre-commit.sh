#!/bin/sh

if [ -z `docker-compose ps -q console` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q console)` ]; then
    RUNNER=""
else
    RUNNER="docker-compose run console "
fi

# shellcheck disable=SC2034
PASS=true

echo "\nValidating PHP-CS-FIXER:\n"

# shellcheck disable=SC2230
# shellcheck disable=SC2039
which ./vendor/bin/php-cs-fixer &> /dev/null
if [ $? -eq 1 ]; then
  echo "\033[41mPlease install PHP-CS-FIXER\033[0m"
  exit 1
fi

if [ -f ./.php-cs-fixer.cache ]; then
    rm ./.php-cs-fixer.cache
fi

$RUNNER./vendor/bin/php-cs-fixer fix --dry-run --verbose --config=./.php-cs-fixer.php ./app/

# shellcheck disable=SC2181
if [ $? -eq 0 ]; then
  echo "\033[32mPHP-CS-FIXER Passed! \033[0m"
else
  echo "\033[41mPHP-CS-FIXER Failed! \033[0m"
  exit 1
fi

# shellcheck disable=SC2028
echo "\033[42mLINTERS TEST SUCCESSFUL\033[0m\n"

exit $?
