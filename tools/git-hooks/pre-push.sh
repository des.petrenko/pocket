#!/bin/sh

if [ -z `docker-compose ps -q console` ] || [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q console)` ]; then
    RUNNER=""
else
    RUNNER="docker-compose run console "
fi

# shellcheck disable=SC2034
PASS=true

echo "\nValidating PHPMD:\n"

# shellcheck disable=SC2230
# shellcheck disable=SC2039
which ./vendor/bin/phpmd &> /dev/null
if [ $? -eq 1 ]; then
  echo "\033[41mPlease install PHP-MD\033[0m"
  exit 1
fi

$RUNNER./vendor/bin/phpmd ./app/ text ./phpmd.xml

# shellcheck disable=SC2181
if [ $? -eq 0 ]; then
  echo "\033[32mPHPMD Passed! \033[0m"
else
  echo "\033[41mPHPMD Failed! \033[0m"
  exit 1
fi

echo "\nValidating PHPSTAN:\n"

# shellcheck disable=SC2230
# shellcheck disable=SC2039
which ./vendor/bin/phpstan &> /dev/null
if [ $? -eq 1 ]; then
  echo "\033[41mPlease install PHPSTAN\033[0m"
  exit 1
fi

$RUNNER./vendor/bin/phpstan analyse

# shellcheck disable=SC2181
if [ $? -eq 0 ]; then
  echo "\033[32mPHPSTAN Passed! \033[0m"
else
  echo "\033[41mPHPSTAN Failed! \033[0m"
  exit 1
fi

# shellcheck disable=SC2028
echo "\033[42mPUSH SUCCEEDED\033[0m\n"

exit $?
